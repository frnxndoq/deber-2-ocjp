/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dodeberocjp;

/**
 *
 * @author FERNANDO
 */
public class Parqueadero implements Interface1{
    
    int contador=1;

    @Override
    public void aceptar(String matricula) {       
        if(contador<10){
            System.out.println("El auto puede entrar, n° matricula: "+ matricula);        
        }      
    }

    @Override
    public void negar(String modelo) {
        
        if(contador>10){
            System.out.println("El auto no puede entrar, modelo: " + modelo);
        }      
    }

    @Override
    public void buscar(String color) {
        
        if(contador==10){
            System.out.println("Buscar auto de Color: "+color);
        }
       
    }

    @Override
    public int contador() {
        
        return contador++;       
    }    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dodeberocjp;

/**
 *
 * @author FERNANDO
 */
public enum Posiciones {
    
    DELANTERO(0), VOLANTE(1), DEFENSA(2), ARQUERO(3);
    
    int numPosicion;
    
    Posiciones(int i){

    this.numPosicion = i;
    }

     
    @Override

  public String toString() {
        String posicion = name();
        posicion = "Posición: " +posicion;
        return posicion;

   }
}

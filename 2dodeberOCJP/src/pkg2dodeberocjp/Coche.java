/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dodeberocjp;

/**
 *
 * @author FERNANDO
 */
public class Coche {
    
    private String matricula;
    private String color;
    private String dueno;
    private String modelo;

    public Coche(String matricula, String color, String dueno, String modelo) {
        this.matricula= matricula;
        this.color = color;
        this.dueno = dueno;
        this.modelo = modelo;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getDueno() {
        return dueno;
    }

    public void setDueno(String dueno) {
        this.dueno = dueno;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    
    //constructor sobrecargado

    public Coche( Coche coche1) {
   
        matricula = coche1.getMatricula();
        color = coche1.getColor();
        modelo = coche1.getModelo();
        dueno = coche1.getDueno();
    }
    
    public Coche (){
        
        matricula = null;
        color = "amarillo";
        modelo = "Chevrolet";
        dueno = null;
    }
    
    public void estadoCoche()
   {
    System.out.println("Determinado por la revisión");
   }
 
    public void kms(int kilometraje)
    {
        System.out.println("kilometros en entero");
    }
    
    public void kms (double kilometraje){
        System.out.println("kilometros con decimal");
    }
}

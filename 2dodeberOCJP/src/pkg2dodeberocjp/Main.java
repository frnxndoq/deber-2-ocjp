/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2dodeberocjp;

/**
 *
 * @author FERNANDO
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Main main = new Main();
      
        centroRevision turno = new centroRevision ("PSD-456", "Negro", "Fernando", "Nissan");

        turno.setIdRevision(1);

        turno.mostrarInformacion();
        
        turno.estadoCoche();
        
        duenocoche propietario=new duenocoche("Fernando", "Quezada");
        turno.dueno(propietario);
        
        //imprimiendo constructores sobrecargados
        
        Coche auto1 = new Coche();
        main.imprimeCoche(auto1);
        
        Coche auto = new Coche ();
        auto.setMatricula("PSD-123");
        auto.setColor("rosa");
        auto.setModelo("Ferrari");
        auto.setDueno("Christian");
        main.imprimeCoche(auto);
        
        Posiciones numPosicion = Posiciones.ARQUERO;

       System.out.print(numPosicion.name());

       //System.out.print("El color es"+numPosicion.name());
        
    }
    
    void imprimeCoche(Coche carro)
   {
     System.out.println("\nMatricula: " + carro.getMatricula() );
     System.out.println("Color: " + carro.getColor() );
     System.out.println("Modelo: " + carro.getModelo());
     System.out.println("Dueño: " + carro.getDueno() +"\n");
   } 
      
}
